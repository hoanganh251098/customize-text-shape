const config = {
    minLines: 5,
    maxLines: 999,
    lineHeight: 1,
    searchThreshold: 1.0001,
    scale: 2,
    color: '#000000'
};

config.defaultSVG = `
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN"
 "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
 width="1183px" height="932px"
 preserveAspectRatio="xMidYMid meet">
<metadata>
Created by potrace 1.16, written by Peter Selinger 2001-2019
</metadata>
<g transform="translate(-20,1079) scale(0.100000,-0.100000)"
fill="#000000" stroke="none">
<path id="targetPath" d="M3015 10754 c-844 -66 -1485 -332 -1970 -818 -532 -533 -796 -1250
-799 -2166 -1 -275 7 -356 54 -590 68 -337 233 -775 405 -1078 207 -364 631
-920 896 -1177 57 -55 1025 -989 2151 -2075 1877 -1810 2054 -1978 2120 -2013
121 -63 273 -61 387 5 32 17 162 135 324 292 150 144 1066 1027 2037 1962 971
934 1821 1758 1890 1829 167 173 279 300 419 475 555 691 863 1366 932 2044
15 157 6 648 -15 806 -81 585 -253 1036 -545 1425 -95 126 -306 343 -432 445
-540 435 -1273 655 -2124 637 -150 -3 -277 -11 -340 -21 -406 -66 -826 -228
-1195 -462 -370 -234 -763 -536 -1033 -794 l-117 -112 -78 73 c-444 423 -1057
860 -1467 1047 -264 120 -511 197 -775 243 -104 18 -172 22 -400 24 -151 1
-297 1 -325 -1z"/>
</g>
    <path id="linesPath" stroke="red" stroke-width="1" d="" pointer-events="none" />
</svg>
`;

config.defaultLyrics = `Heart beats fast Colors and promises How to be brave? How can I love when I'm afraid to fall? But watching you stand alone All of my doubt suddenly goes away somehow One step closer I have died every day waiting for you Darling, don't be afraid I have loved you for a thousand years I'll love you for a thousand more Time stands still Beauty in all she is I will be brave I will not let anything take away What's standing in front of me Every breath Every hour has come to this One step closer I have died every day waiting for you Darling, don't be afraid I have loved you for a thousand years I'll love you for a thousand more And all along I believed I would find you Time has brought your heart to me I have loved you for a thousand years I'll love you for a thousand more One step closer One step closer I have died every day waiting for you Darling don't be afraid I have loved you for a thousand years I'll love you for a thousand more And all along I believed I would find you Time has brought your heart to me I have loved you for a thousand years I'll love you for a thousand more
`;

// config.font = `https://fonts.gstatic.com/s/indieflower/v12/m8JVjfNVeKWVnh3QMuKkFcZVaUuH.woff2`;
// config.font = 'https://fonts.gstatic.com/s/monoton/v10/5h1aiZUrOngCibe4TkHLQg.woff2';
// config.font = 'https://fonts.gstatic.com/s/ralewaydots/v9/6NUR8FifJg6AfQvzpshgwJ8UzvVE.woff2';
config.font = 'https://fonts.gstatic.com/s/fuggles/v1/k3kQo8UEJOlD1hpOfdnoLg.woff2';
// config.font = 'http://dev.goodsmize.com/design-upload-v2/resources/text-shape-fonts/American%20Typewriter%20Regular.ttf'
